int arabic_to_roman(long arabic_val, char *roman_out_str);
int roman_to_arabic(const char roman_str[]);
int add_roman(const char *n1, const char *n2, char *result);
int sub_roman(const char *n1, const char *n2, char *result);
 
