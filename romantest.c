/*********************************************
 * Test Roman numerals <--> Arabic numbers   *
 *            conversion routines            *
 *                                           *
 *              Ross Archer                  *
 *              20 June 2016                 *
 *                                           *
 *********************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "roman_math.h"

#define MAX_ROMAN_NUMERAL_LEN 255 

/* Note: for addition and subtraction, it might be possible to perform
 * a series of string transformations and do all the addition and 
 * subtraction in the Roman numeral domain.  That's very clever, but 
 * having conversion routines in each direction is probably more useful,
 * as we could also expand this to support multiplication, possibly even 
 * integer division.
 */

int main (void)
{
    
    char str[MAX_ROMAN_NUMERAL_LEN+1];
        char str2[MAX_ROMAN_NUMERAL_LEN+1];
        char result[MAX_ROMAN_NUMERAL_LEN+1];

    printf("\nBegin tests:\n");
    /* Test block A-0: total nonsense input returns -1 */
    printf("Testing nonsense input values\n");
    assert(roman_to_arabic("cat") == -1);
    assert(roman_to_arabic("TRUE") == -1);
    assert(roman_to_arabic("AE35 antenna unit") == -1);
    assert(roman_to_arabic("") == -1);
    printf("Passed nonsense input values\n");
    /* Test block A-1, single digit Roman numerals to values test */
    printf("Testing single digit Roman numerals\n");
    assert(roman_to_arabic("I") == 1);
    assert(roman_to_arabic("V") == 5);
    assert(roman_to_arabic("X") == 10);
    assert(roman_to_arabic("L") == 50);
    assert(roman_to_arabic("C") == 100);
    assert(roman_to_arabic("D") == 500);
    assert(roman_to_arabic("M") == 1000);
    printf("Passed single digit Roman numerals\n");

    /* Test block A-2, purely additive cases with multiple Roman numerals */
    printf("Testing multiple digit additive cases\n"); 
    assert(roman_to_arabic("III") == 3);
    assert(roman_to_arabic("VI") == 6);
    assert(roman_to_arabic("XI") == 11);
    assert(roman_to_arabic("LX") == 60);
    assert(roman_to_arabic("CL") == 150);
    assert(roman_to_arabic("DC") == 600);
    assert(roman_to_arabic("MD") == 1500);
    assert(roman_to_arabic("MDCLXVI") == 1666);
    printf("passed multiple digit additive tests\n");
    
    /* Test block A-3, Subtractive and additive values together */
    printf("Subtractive cases - including mixed additive and subtractive\n");
    assert(roman_to_arabic("IV") == 4);
    assert(roman_to_arabic("IX") == 9);
    assert(roman_to_arabic("XL") == 40);
    assert(roman_to_arabic("CD") == 400);
    assert(roman_to_arabic("MCDXLVI") == 1446);
    assert(roman_to_arabic("MCMLXIX") == 1969);
    printf("passed subtractive cases\n");
   
    /* Test block B - Arabic value to roman numerals */
    /* Be insane and test each function and inverse from 1 to 3999 */
    int i, j;
    int val;
    printf("Arabic to Roman to Arabic tests\n");
    for (i = 1; i <= 3999; i++) {
        assert(arabic_to_roman(i, str) != -1);
        printf("%d:\t\t%s\n", i, str);
        assert(roman_to_arabic(str) == i);
    }
    printf("Passed arabic -> roman -> arabic 1...3999 all values\n");
            
    /* Test block C - Addition functions */
    printf("Testing addition funtions, semi-exhaustively...\n");
    for (i = 1; i < 1999; i++) {
        for (j = 1; j < 1000; j++) {
            arabic_to_roman(i, str);
            arabic_to_roman(j, str2);
            add_roman(str, str2, result);
            val = roman_to_arabic(result);
            assert(val == (i + j)); 
        } /* for j */
    } /* for i */
    printf("Passed addition tests\n");
    /* Test block E - Subtraction functions */
    printf("Testing subtraction functions, semi-exhaustively...\n");
    for (i = 3999; i > 3; i--) {
        for (j = i-1; j >= 1; j--) {
            arabic_to_roman(i, str);
            arabic_to_roman(j, str2);
            val = sub_roman(str, str2, result);
            assert(val == 0);
            val = roman_to_arabic(result);
            assert(val == (i - j));
        } /* for j */
    } /* for i */
    printf("Passed subtraction tests\n"); 
    printf("ALL TESTS PASSED\n");
}

