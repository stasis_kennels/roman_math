/*********************************************
 * Convert Roman numerals to Arabic numbers  *
 *                                           *
 *              Ross Archer                  *
 *              20 June 2016                 *
 *                                           *
 *********************************************/

#include <stdio.h>
#include <string.h>
#include "roman_math.h"

typedef struct {
    long value;
    const char *roman;
} roman_lookup_entry_t;

/* Note: this lookup table isn't minimal, but it does reduce complexity to
 * subtracting the largest entry from the remaining value, and repeating until
 * the value is 0. Computationally very simple but table is slighly larger.
 */

const roman_lookup_entry_t roman_lookup_table[] = {
    {1000, "M"}, {900,  "CM"}, {800,  "DCCC"}, {700,  "DCC"}, {600,  "DC"},
    {500,  "D"}, {400,  "CD"}, {300,  "CCC"}, {200,  "CC"}, 
    {100,  "C"}, {90,   "XC"}, {80,   "LXXX"}, {70,   "LXX"}, {60,   "LX"},
    {50,   "L"}, {40,   "XL"}, {30,   "XXX"}, {20,   "XX"}, 
    {10,   "X"}, {9,    "IX"}, {8,    "VIII"}, {7,    "VII"}, {6,    "VI"},
    {5,    "V"}, {4,    "IV"}, {3,    "III"}, {2,    "II"}, {1,    "I"}
};

const int table_entries = sizeof(roman_lookup_table) / sizeof(roman_lookup_entry_t);


/* to convert Roman numerals to their numeric value */
static int char_to_arabic (char ch)
{
    switch(ch) {
    case 'I': 
	return 1;
    case 'V':
	return 5;
    case 'X':
        return 10;
    case 'L':
        return 50;
    case 'C':
        return 100;
    case 'D':
        return 500;
    case 'M':
        return 1000;
    default:
        return -1;
    }

}

int roman_to_arabic(const char romanstr[])
{
    int lval, rval;
    int sum = 0;
    int i = 0;
    int len = strlen(romanstr);

    if (len == 0)
        return -1;
    
    while (i < len) {
        lval = char_to_arabic(romanstr[i++]);
        if (i < len)
            rval = char_to_arabic(romanstr[i]);
        else
            rval = 0;
        if ((lval < 0) || (rval < 0))
            return -1;

        if (lval >= rval) {
            sum = sum + lval;
        } else {
            sum = sum + (rval - lval);
            ++i;
        }
    }
    return sum;
}

/* How many entries are there to search in the table? */
const int entries = sizeof(roman_lookup_table) / sizeof(roman_lookup_entry_t);


/* Convert an Arabic number into a string representing the Roman numeral */
int arabic_to_roman (long arabic_val, char *roman_str)
{
    int i;

    /* Obviously, there are no negative or zero values allowed */
    if (arabic_val < 1) {
        strcpy(roman_str, "ERROR");
        return -1;
    }

    *roman_str = (char) 0; /* Start off by returning nothing */

    while (arabic_val >= 1) {
        /* Find largest table entry and subtract, concatenate  */
        for (i = 0; i < entries; i++) {
            if (arabic_val >= roman_lookup_table[i].value) {
                arabic_val -= roman_lookup_table[i].value;
                strcat(roman_str, roman_lookup_table[i].roman);
                break;  /* Bail and find largest value again until 0 */
            }
        } /* for */
    }
    return 0;
}

/* Convert each string to Arabic, do the math, then convert it back! */
int add_roman(const char *n1, const char *n2, char *result)
{
    int v1, v2;
    
    v1 = roman_to_arabic(n1);
    v2 = roman_to_arabic(n2);
    if ((v1 == -1) || (v2 == -1)) 
        return -1;
    
    if (arabic_to_roman((v1+v2), result) == -1) {
        return -1;
    }
    return 0;
}

/* Convert each string to Arabic, do the math, then convert it back! */
int sub_roman(const char *n1, const char *n2, char *result)
{
  int v1, v2;

    v1 = roman_to_arabic(n1);
    v2 = roman_to_arabic(n2);
    if ((v1 == -1) || (v2 == -1))
        return -1;

    if (arabic_to_roman((v1-v2), result) == -1) {
        return -1;
    }
    return 0;
} 
 
