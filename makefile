# Roman number silliness :) functions 
#
#
CC=gcc
CFLAGS= -Wall -Wpedantic -I.
DEPS= roman_math.h
OBJ=  romantest.o roman_math.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) 

romantest: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)


